import Foundation
import CCLServices

struct NotificationsScreenData {
    
    struct Notification {
        let time: String?
        let title: String?
        let body: String?
        let isRead: Bool?
    }
    
    struct NotificationsDay {
        let date: String
        let notifications: [Notification]
    }
    
    var days: [NotificationsDay]
    
    init(response: [NotificationsResponsePayload]) {
        self.days = []
        var currentDayNotifications = [Notification]()
        var currentDay = ""
        for notification in response {
            let notificationDay = CCLDateTimeManager.shared.localDateString(from: notification.createdAt ?? "", serverStringFormat: .long, outputFormat: "MMMM dd yyyy") ?? ""
            let notificationTime = CCLDateTimeManager.shared.localDateString(from: notification.createdAt ?? "", serverStringFormat: .long, outputFormat: "h:mm a")
            if currentDay == "" {
                currentDay = notificationDay
            }
            if notificationDay != currentDay {
                self.days.append(NotificationsDay(date: currentDay, notifications: currentDayNotifications))
                currentDay = notificationDay
                currentDayNotifications.removeAll()
            }
             let title =  String(format: notification.notification?.titleLocKey?.localized() ?? "", arguments: notification.notification?.titleLocArgs ?? [])
            let body = String(format: notification.notification?.bodyLocKey?.localized() ?? "", arguments: notification.notification?.bodyLocArgs ?? [])
            currentDayNotifications.append(Notification(time: notificationTime,
                                                        title: title,
                                                        body: body,
                                                        isRead: notification.isRead ?? true))
        }
        //for the last day:
        self.days.append(NotificationsDay(date: currentDay, notifications: currentDayNotifications))
    }
}

