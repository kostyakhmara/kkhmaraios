import UIKit
import DZNEmptyDataSet

class NotificationController: UIViewController {
    
    static func storyboardInstance() -> NotificationController? {
        let storyboard = UIStoryboard(name: "NotificationScreen", bundle: nil)
        return storyboard.instantiateInitialViewController() as? NotificationController
    }

    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "NotificationScreenCell", bundle: nil), forCellReuseIdentifier: "NotificationScreenCell")
            tableView.estimatedRowHeight = 150.0
            tableView.delegate = self
            tableView.dataSource = self
            tableView.emptyDataSetSource = self
        }
    }
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    let viewModel = NotificationsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        bindViewModel()
        setupView()
        setupNotificationsObservers()
        viewModel.fetchNotifications()
    }
    
    private func setupView() {
        view.backgroundColor = ColorPalette.tableViewBackground
        tableView.isHidden = true
        activityView.hidesWhenStopped = true
        activityView.startAnimating()
    }
    
    private func bindViewModel() {
        viewModel.bind(outputs: NotificationsViewModel.Outputs(
            notificationsFetched: { [weak self] in
                self?.tableView.isHidden = false
                self?.activityView.stopAnimating()
                self?.tableView.reloadData()
            
        }, notificationsFetchError: { [weak self] (error) in
            self?.showInfoAlert(title: "Error".localized(), msg: error, doAction: nil)
        }))
    }
    
    private func setupNavigationBar() {
        title = "Notifications".localized()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneButtonTapped))
        navigationItem.leftBarButtonItem = doneButton
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: Configuration.navigationHeaderFont]
    }
    
    private func setupNotificationsObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.newNotificationsReceived), name: CCLNotification.newNotificationReceivedNotification, object: nil)
    }
    
    //Actions:
    
    @objc
    private func newNotificationsReceived() {
        viewModel.fetchNotifications()
    }
    
    @objc
    private func doneButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
}

extension NotificationController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.viewData?.days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.viewData?.days[section].notifications.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let day = viewModel.viewData?.days[section]
        return day?.date ?? ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationScreenCell.identifier) as! NotificationScreenCell
        let day = viewModel.viewData?.days[indexPath.section]
        let notification = day?.notifications[indexPath.row]
        cell.setup(notification: notification)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension NotificationController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No notifications to show".localized(), attributes: [NSAttributedString.Key.font : UIFont(name: "Helvetica-Bold", size: 22)!])
    }
}
