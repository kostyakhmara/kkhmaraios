import Foundation
import CCLServices

class NotificationsViewModel {
    
    struct Outputs {
        let notificationsFetched: () -> Void
        let notificationsFetchError: (String) -> Void
    }
    
    private let endpoints = APIEndpoints.shared
    
    var outputs: Outputs?
    var viewData: NotificationsScreenData?
    
    func bind(outputs: Outputs) {
        self.outputs = outputs
    }
    
    func fetchNotifications() {
        guard let token = UserDefaultsGetter.get(.token) else {
            _log(name: "fetchNotifications", description: "No token", type: .error)
            return
        }
        endpoints.getNotifications(token: token) { (result) in
            switch result {
            case .success(let response):
                guard let status = response.status, status == "ok" else {
                    self.outputs?.notificationsFetchError(APIEndpoints.shared.getTranslateByKeys(errorMessages: response.errorMessage) ?? "")
                    return
                }
                self.viewData = NotificationsScreenData(response: response.payload ?? [])
                self.outputs?.notificationsFetched()
            case .failure(let error):
                self.outputs?.notificationsFetchError(error.localizedDescription)
            }
        }
    }
}
